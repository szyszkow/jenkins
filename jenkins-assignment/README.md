# Jenkins deployment

### Jenkins URL
`http://ec2-3-127-66-98.eu-central-1.compute.amazonaws.com:8080`

### Deployment

1. `kubectl create namespace jenkins`
2. `kubectl apply -f pvc.yaml`
3. `kubectl apply -f svc.yaml`
4. `kubectl apply -f deployment.yaml`

### Wait for Jenkins Pod to get up
5. `kubectget pods -n jenkins --watch`
```jenkins-5b96865c78-z9h5m 1/1 Running 0  10s```

### Get Jenkins password
6. `kubectl exec --namespace jenkins -it svc/jenkins -c jenkins -- /bin/cat /run/secrets/additional/chart-admin-password && echo`

### Connect to Jenkins

7. `kubectl port-forward -n jenkins svc/jenkins 8080:8080 --address='0.0.0.0'`
